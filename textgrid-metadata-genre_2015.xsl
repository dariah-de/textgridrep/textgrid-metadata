<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:dct="http://purl.org/dc/terms/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:gndo="http://d-nb.info/standards/elementset/gnd#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rdfs="http://www.w3.org/1999/02/22-rdf-schema-ns#" xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="xsl xs dct foaf gndo skos rdf rdfs" version="2.0">

    <xsl:output method="xml" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

    <xsl:template match="/">
        <html xml:lang="en">
            <head>
                <title>
                    <xsl:value-of select="//dct:title"/>
                </title>
                <link
                    href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css"
                    rel="stylesheet"/>
                <link href="http://textgrid.de/fileadmin/templates/textgrid2012/css/textgrid.css"
                    rel="stylesheet"/>
            </head>
            <body>
                <table summary="border-spacing:2em">
                    <tr>
                        <td style="width:20%;vertical-align:top">
                            <img style="width:100%">
                                <xsl:attribute name="alt"
                                    select="//skos:conceptScheme/foaf:logo/@rdf:resource"/>
                                <xsl:attribute name="src"
                                    select="//skos:conceptScheme/foaf:logo/@rdf:resource"/>
                            </img>
                        </td>
                        <td style="background-color:#eee;padding:20px;">
                            <h1>
                                <xsl:value-of select="//dct:title"/>
                            </h1>
                            <p>
                                <xsl:value-of select="//skos:conceptScheme/dct:created"/>
                            </p>
                            <div>
                                <dl>
                                    <dt>Namespace:</dt>
                                    <dd>
                                        <a
                                            href="http://textgrid.info/namespaces/metadata/genre/2015"
                                            >http://textgrid.info/namespaces/metadata/genre/2015</a>
                                    </dd>
                                    <dt>Contributors:</dt>
                                    <xsl:for-each select="//skos:conceptScheme/dct:contributor">
                                        <dd>
                                            <a>
                                                <xsl:attribute name="href">
                                                  <xsl:value-of select="./rdf:value"/>
                                                </xsl:attribute>
                                                <xsl:value-of select="./rdfs:label"/>
                                            </a>
                                        </dd>
                                    </xsl:for-each>
                                    <dt>License:</dt>
                                    <dd>
                                        <a>
                                            <xsl:attribute name="href">
                                                <xsl:value-of
                                                  select="//skos:conceptScheme/dct:license/@rdf:resource"
                                                />
                                            </xsl:attribute>
                                            <xsl:value-of
                                                select="//skos:conceptScheme/dct:license/@rdf:resource"
                                            />
                                        </a>
                                    </dd>
                                </dl>
                            </div>
                            <hr/>
                            <h2>Description</h2>
                            <p>
                                <xsl:value-of select="//skos:conceptScheme/dct:description"/>
                            </p>
                            <div>
                                <h2>Genre terms</h2>
                                <table style="border:1px solid #333;">
                                    <xsl:for-each select="//skos:Concept">
                                        <tr>
                                            <td style="padding:5px;background-color:#FFF">
                                                <xsl:value-of select="./skos:prefLabel"/>
                                            </td>
                                        </tr>
                                    </xsl:for-each>
                                </table>
                            </div>
                        </td>
                        <td style="width:20%"/>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
