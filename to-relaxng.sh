#!/bin/bash

mkdir -p relaxng

for xsd in rdf.xsd textgrid-metadata-agent_2010.xsd textgrid-metadata-language_v2_2010.xsd textgrid-metadata-script_2010.xsd textgrid-metadata_2010.xsd
do
    name=`basename "$xsd" .xsd`
    sed -e '/schemaLocation/s,http://www.textgrid.info/schemas/,,g' "$xsd" \
	| xsltproc xsdtorng/XSDtoRNG.xsl - \
	| xsltproc -o "relaxng/${name}.rng" fix-rng.xsl - 

    ( cd relaxng && trang ${name}.rng ${name}.rnc )
done
